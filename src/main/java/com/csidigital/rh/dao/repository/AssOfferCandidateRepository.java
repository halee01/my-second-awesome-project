package com.csidigital.rh.dao.repository;

import com.csidigital.rh.dao.entity.OfferCandidate;
import com.csidigital.rh.dao.entity.Candidate;
import com.csidigital.rh.dao.entity.Offer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AssOfferCandidateRepository extends JpaRepository<OfferCandidate, Long> {
     List<OfferCandidate> findByOffer(Offer offer);
     List<OfferCandidate> findByCandidate(Candidate candidate);
}
