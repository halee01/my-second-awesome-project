package com.csidigital.rh.dao.entity;

import com.csidigital.rh.shared.enumeration.*;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor

public class Employee {
    @jakarta.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;
    private String lastName;
    private String firstName;
    @Enumerated(EnumType.STRING)
    private Civility civility;
    private Title title;
    @Enumerated(EnumType.STRING)
    private EmployeeType employeeType;
    private LocalDate birthDate;
    private String emailOne;
    private String emailTwo;
    private int phoneNumberOne;
    private int phoneNumberTwo;
    private String adress;
    private int postCode;
    private int city;
    @Enumerated(EnumType.STRING)
    private Country country;
    @Enumerated(EnumType.STRING)
    private MaritalSituation maritalSituation;
    private int recommendationType ;
    private int experience ;
    private String experienceDetails ;
}
