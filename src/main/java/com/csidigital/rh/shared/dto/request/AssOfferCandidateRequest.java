package com.csidigital.rh.shared.dto.request;

import com.csidigital.rh.dao.entity.Evaluation;
import com.csidigital.rh.shared.enumeration.CandidatStatus;
import com.csidigital.rh.shared.enumeration.ExperienceLevel;
import jakarta.persistence.Embeddable;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapsId;
import lombok.Data;

import java.time.LocalDate;

@Data

public class AssOfferCandidateRequest {

    private LocalDate applicationDate;
    private ExperienceLevel experienceLevel;
    private CandidatStatus candidatStatus;
   // private Long evaluationId;

    private Long offerNum;
    private Long candidateNum;


}
