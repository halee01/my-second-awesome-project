package com.csidigital.rh.shared.dto.response;

import com.csidigital.rh.shared.enumeration.*;
import lombok.Data;

import java.time.LocalDate;
@Data
public class EmployeeResponse {
    private Long Id ;
    private String lastName;
    private String firstName;
    private Civility civility;
    private Title title;
    private EmployeeType employeeType;
    private LocalDate birthDate;
    private String emailOne;
    private String emailTwo;
    private int phoneNumberOne;
    private int phoneNumberTwo;
    private String adress;
    private int postCode;
    private int city;
    private Country country;
    private MaritalSituation maritalSituation;
    private int recommendationType ;
    private int experience ;
    private String experienceDetails ;
}
