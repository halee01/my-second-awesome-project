package com.csidigital.rh.shared.dto.request;

import com.csidigital.rh.dao.entity.OfferCandidate;
import lombok.Data;

@Data
public class EvaluationRequest {
    private int globalAppreciation;
    private Long offerCandidateId;
}
