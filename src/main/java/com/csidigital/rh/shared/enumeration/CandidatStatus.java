package com.csidigital.rh.shared.enumeration;

public enum CandidatStatus {
    IN_PROCESS,IN_PROGRESS,PRE_QUALIFIED,TOP_PROFILES,CONVERTED_TO_RESSOURCE,DO_NOT_CONTACT,ARCHIVE
}
